import click
import numpy as np
import pandas as pd


@click.command()
@click.argument("filename", type=click.Path(exists=True))
def convert(filename):
    """Convert SWC file to STL"""

    # See SWC file specification at:
    # http://www.neuronland.org/NLMorphologyConverter/MorphologyFormats/SWC/Spec.html
    ColNames = [
        "SampleNumber",
        "StructureIdentifier",
        "x",
        "y",
        "z",
        "radius",
        "ParentSample",
    ]
    ColTypes = {
        "SampleNumber": np.int_,
        "StructureIdentifier": np.int_,
        "x": np.float_,
        "y": np.float_,
        "z": np.float_,
        "radius": np.float_,
        "ParentSample": np.int_,
    }
    tab = pd.read_table(
        filename,
        comment="#",
        header=None,
        names=ColNames,
        index_col="SampleNumber",
        dtype=ColTypes,
    )
    coord = tab[["x", "y", "z"]]
    click.echo(coord.head())

    filename_out = filename.replace(".swc", ".asc")
    coord.to_csv(
        path_or_buf=filename_out, sep=" ", header=False, index=False, decimal="."
    )
    click.echo("File converted to: {}".format(filename_out))


if __name__ == "__main__":
    convert()
